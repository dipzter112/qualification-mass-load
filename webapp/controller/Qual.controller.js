sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/ui/core/message/Message"
], function (Controller, MessageToast, Fragment, Message) {
	"use strict";

	return Controller.extend("am.qualificationUpload.controller.Qual", {
		onInit: function () {
			var oMessageManager;
			oMessageManager = sap.ui.getCore().getMessageManager();
			this.getView().setModel(oMessageManager.getMessageModel(), "message");
			oMessageManager.registerObject(this.getView(), true);
			//this.getView().byId("idfileUploader").setSameFilenameAllowed(true);
		},
		handleUploadComplete: function (e) {
			var that = this;
			var fileUploader = this.getView().byId("idfileUploader");
			var domRef = fileUploader.getFocusDomRef();
			var file = domRef.files[0];
			var reader = new FileReader();

			this.getView().getModel("qualModel").refresh(true, true, "qual");
			sap.ui.getCore().getMessageManager().removeAllMessages();
			this.byId("messageButton").setVisible(false);
			this.byId("messageButton").setText(0);
			//this.byId("TimeColumn").setVisible(true);

			if (!this._dialog) {
				this._dialog = sap.ui.xmlfragment("am.qualificationUpload.view.BusyDialog", this);
				this.getView().addDependent(this._dialog);
			}
			var preText = this.getView().getModel("i18n").getResourceBundle().getText("preText");
			sap.ui.getCore().byId("BusyText").setText(preText);
			this._dialog.open();

			reader.onload = function (oEvent) {

				jQuery.sap.require("am.qualificationUpload.util.xlsx");
				var workbook = XLSX.read(oEvent.target.result, {
					type: "binary",
					cellDates: true
				});

				var result = {};
				workbook.SheetNames.forEach(function (sheetName) {
					var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
						header: ""
					});
					if (roa.length) result[sheetName] = roa;
				});

				var i;
				var qualModel = that.getView().getModel("qualModel");
				qualModel.setRefreshAfterChange(true);
				qualModel.setDeferredGroups(["qual"]);

				for (i = 0; i < result.Sheet1.length; i++) {

					var obj = {};
					obj.Otype = "CP";
					obj.Objid = result.Sheet1[i].AMEI.toString();
					obj.QId = result.Sheet1[i].Qualification.slice(0, 8);
					var newDate = result.Sheet1[i].Date;
					newDate.setDate(newDate.getDate() + 1);
					obj.DateQuali = newDate.toISOString().slice(0, 10).replace(/\-/g, '');
					//obj.DateQuali = result.Sheet1[i].Date.toISOString().slice(0, 10).replace(/\-/g, '');
					//obj.DateQuali = obj.DateQuali.split("/").reverse().join("");

					// if (results.Sheet1[i].hasOwnProperty("PROFICIENCY") === true) {
					// 	obj.ProfId = results.Sheet1[i].PROFICIENCY;
					// }
					// //checks if the column exists(for the other file)
					// if (results.Sheet1[i].hasOwnProperty("HOURS") === true) {
					// 	obj.HoursToLoad = results.Sheet1[i].HOURS;
					// } else {
					// 	that.getView().byId("TimeColumn").setVisible(false);
					// }
					// if (results.Sheet1[i].hasOwnProperty("DELIVERY_METHOD") === true) {
					// 	obj.DeliveryMethod = results.Sheet1[i].DELIVERY_METHOD;
					// }
					// if (results.Sheet1[i].hasOwnProperty("TRAINING_TITLE") === true) {
					// 	obj.TrainingTitle = results.Sheet1[i].TRAINING_TITLE;
					// }
					qualModel.create("/MassLoadQualificationSet", obj, {
						groupId: "qual"
					});

				}
				qualModel.submitChanges({
					groupId: "qual",
					success: that.mySuccessHandler.bind(that),
					error: that.myErrorHandler.bind(that)

				});

			};
			reader.readAsBinaryString(file);
		},
		mySuccessHandler: function (oData) {
			var that = this;
			this.byId("submitButton").setEnabled(true);
			var resModel = new sap.ui.model.json.JSONModel();
			var qualModel = this.getView().getModel("qualModel");
			var qualData = qualModel.getProperty("/");

			$.map(qualData, function (obj, index) {
					if (!index.includes("MassLoadQualificationSet")) {
						delete qualData[index];

					}
				}

			);

			resModel.setData(qualData);
			this.getView().byId("qualTable").setModel(resModel, "resModel");
			//set message handler.....
			var i = 0;
			$.map(qualData, function (obj, index) {
				//set the button text and visible value.
				if (obj.Status === "Error") {
					i++;
					var oMessage = new Message({
						message: obj.Message,
						type: sap.ui.core.MessageType.Error,
						target: "/Dummy",
						processor: that.getView().getModel("message")
					});
					sap.ui.getCore().getMessageManager().addMessages(oMessage);
				} else if (obj.Status === "Success" && obj.Message !== "") {
					i++;
					oMessage = new Message({
						message: obj.Message,
						type: sap.ui.core.MessageType.Success,
						target: "/Dummy",
						processor: that.getView().getModel("message")
					});
					sap.ui.getCore().getMessageManager().addMessages(oMessage);
				}
			});

			if (i > 0) {
				this.byId("messageButton").setVisible(true);
				this.byId("messageButton").setText(i);
			}
			sap.ui.core.BusyIndicator.hide();
			this._dialog.close();

		},

		mySuccessHandlerSubmit: function (oData) {
			var that = this;
			this.byId("submitButton").setEnabled(true);
			var resModel = new sap.ui.model.json.JSONModel();
			var qualModel = this.getView().getModel("qualModel");
			var qualData = qualModel.getProperty("/");
			var isError = false;

			$.map(qualData, function (obj, index) {
					if (obj.Status === "Success") {
						//
						var sPath = "/" + index;

						that.getView().getModel("qualModel").remove(sPath, {
							success: that.mySuccessHandlerRemove.bind(that),
							error: that.myErrorHandlerRemove.bind(that)
						});

						delete qualData[index];

					} else {
						isError = true;
					}
				}

			);
			if (isError === true) {
				var text = this.getView().getModel("i18n").getResourceBundle().getText("submitError");
			} else {
				text = this.getView().getModel("i18n").getResourceBundle().getText("submitSuccess");
				//disable submit button and reset fileuploader 
				sap.ui.getCore().getMessageManager().removeAllMessages();
				this.byId("messageButton").setVisible(false);
				this.byId("messageButton").setText(0);
				this.byId("submitButton").setEnabled(false);
				this.getView().byId("idfileUploader").setValue("");

			}

			sap.m.MessageToast.show(text, {
				duration: 5000, // default
				width: "30em", // default
				my: "center center", // default
				at: "center center", // default
				autoClose: true,
			});

			resModel.setData(qualData);
			this.getView().byId("qualTable").setModel(resModel, "resModel");
			//set message handler.....
			var i = 0;
			$.map(qualData, function (obj, index) {
				//set the button text and visible value.
				if (obj.Status === "Error") {
					i++;
					var oMessage = new Message({
						message: obj.Message,
						type: sap.ui.core.MessageType.Error,
						target: "/Dummy",
						processor: that.getView().getModel("message")
					});
					sap.ui.getCore().getMessageManager().addMessages(oMessage);
				} else if (obj.Status === "Success" && obj.Message !== "") {

				}
			});

			if (i > 0) {
				this.byId("messageButton").setVisible(true);
				this.byId("messageButton").setText(i);
			}
			sap.ui.core.BusyIndicator.hide();
			this._dialog.close();

		},

		myErrorHandler: function (oError) {
			this._dialog.close();
			sap.m.MessageToast.show(oError.response.body, {
				duration: 5000, // default
				width: "30em", // default
				my: "center center", // default
				at: "center center", // default
				autoClose: true,
			});
		},

		handleTypeMissmatch: function (oEvent) {
			var aFileTypes = oEvent.getSource().getFileType();
			jQuery.each(aFileTypes, function (key, value) {
				aFileTypes[key] = "*." + value;
			});
			var sSupportedFileTypes = aFileTypes.join(", ");
			var msg = "The file type *." + oEvent.getParameter("fileType") +
				" is not supported. Choose one of the following types: " +
				sSupportedFileTypes;

			sap.m.MessageToast.show(msg, {
				duration: 5000, // default
				width: "30em", // default
				my: "center center", // default
				at: "center center", // default
				autoClose: true,
			});
		},

		onPress: function (e) {
			if (e.getSource().getBindingContext("resModel").getProperty("Status") === "Error") {
				var msg = e.getSource().getBindingContext("resModel").getProperty("Message");

				sap.m.MessageToast.show(msg, {
					duration: 5000, // default
					width: "30em", // default
					my: "center center", // default
					at: "center center", // default
					autoClose: true,
				});

			}
		},

		handleEditPress: function (oEvent) {
			var editDialog = this.byId("editDialog");
			var item = oEvent.getSource();
			var obj = item.getBindingContext("resModel").getObject();
			editDialog.setBindingContext(item.getBindingContext("resModel"), "resModel");
			this.getView().byId("qualList").setSelectedKey(obj.QId);

			var qualSearchModel = this.getView().byId("qualListNew").getModel("qualSearchModel");
			if (qualSearchModel === undefined || qualSearchModel === null) {

				qualSearchModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZGW_MASS_LOAD_QUALIFICATIONS_SRV/");

				var oData = qualSearchModel.read("/QualSearchSet()", {
					success: this.mySuccessHandlerEdit.bind(this),
					error: this.myErrorHandlerEdit.bind(this)
				});
			} else {

				editDialog.open();
			}

		},

		mySuccessHandlerQual: function (oData) {
			//bind the oData to the dropdown box...
			var qualSearchModel = new sap.ui.model.json.JSONModel();
			qualSearchModel.setData(oData);
			this.getView().byId("qualList").setModel(qualSearchModel, "qualSearchModel");
			this.byId("editDialog").open();
		},

		myErrorHandlerQual: function (oError) {
			//
		},

		onMessagePopoverPress: function (oEvent) {
			this._getMessagePopover().openBy(oEvent.getSource());
		},

		_getMessagePopover: function () {
			if (!this._oMessagePopover) {
				this._oMessagePopover = sap.ui.xmlfragment(this.getView().getId(), "am.qualificationUpload.view.MessagePopover", this);
				this.getView().addDependent(this._oMessagePopover);
			}
			return this._oMessagePopover;
		},

		handleSubmit: function (oEvent) {

			sap.ui.getCore().getMessageManager().removeAllMessages();
			this.byId("messageButton").setVisible(false);
			this.byId("messageButton").setText(0);
			//this.byId("submitButton").setEnabled(false);

			if (!this._dialog) {
				this._dialog = sap.ui.xmlfragment("am.qualificationUpload.view.BusyDialog", this);
				this.getView().addDependent(this._dialog);
			}

			var qualModel = this.getView().getModel("qualModel");
			qualModel.setDeferredGroups(["qualupdate"]);
			var qualData = qualModel.getProperty("/");

			var isError = false;
			var isData = false;

			$.map(qualData, function (obj, index) {
				isData = true;
				if (obj.Status === "Error") {
					isError = true;
					return;
				}

			});

			if (isError === true) {
				sap.m.MessageToast.show("The load still contains errors; please remove or correct them", {
					duration: 5000, // default
					width: "30em", // default
					my: "center center", // default
					at: "center center", // default
					autoClose: true,
				});

				return;
			}

			if (isData === false) {

				sap.m.MessageToast.show("Nothing to submit...", {
					duration: 5000, // default
					width: "30em", // default
					my: "center center", // default
					at: "center center", // default
					autoClose: true,
				});

				return;
			}

			var loadText = this.getView().getModel("i18n").getResourceBundle().getText("loadText");
			sap.ui.getCore().byId("BusyText").setText(loadText);
			this._dialog.open();

			qualModel.refresh(true, true, "qual");
			$.map(qualData, function (obj, index) {

				if (obj.Status !== "Error" && index.includes("MassLoadQualificationSet")) {
					//qualModel.update("/MassLoadQualificationSet(Otype='" + obj.Otype + "',Objid='" + obj.Objid + "',QId='" + obj.QId + "')", obj, { groupId: "qualupdate" });
					qualModel.create("/MassLoadQualificationSet", obj, {
						groupId: "qualupdate"
					});
				}
			});

			qualModel.submitChanges({
				groupId: "qualupdate",
				success: this.mySuccessHandlerSubmit.bind(this),
				error: this.myErrorHandler.bind(this)
			});
		},
		handleDownloadTemplateOpen: function (oEvent) {
			var oButton = oEvent.getSource();
			if (!this._actionSheet) {
				this._actionSheet = sap.ui.xmlfragment(
					"am.qualificationUpload.view.TemplateSelect",
					this
				);
				this.getView().addDependent(this._actionSheet);
			}

			this._actionSheet.openBy(oButton);
		},
		handleQualDownload: function (oEvent) {
			sap.m.URLHelper.redirect("https://www.myarcelormittal.com/fcekr/component/directopen?objectId=0901bad780a8207e&status=active");

		},
		handleHoursDownload: function (oEvent) {
//			sap.m.URLHelper.redirect("https://www.myarcelormittal.com/fcekr/component/directopen?objectId=0901bad78099e232&status=active");

		},

		handleHelpPress: function (oEvent) {

			if (!this._dialogHelp) {
				this._dialogHelp = sap.ui.xmlfragment("am.qualificationUpload.view.Help", this);
				this.getView().addDependent(this._dialogHelp);
			}
			this._dialogHelp.open();
		},

		onCloseHelp: function (oEvent) {
			this._dialogHelp.close();
		},
		handleCloseEdit: function (oEvent) {
			this.byId("editDialog").close();

		},

		onAmeiCheck: function (oEvent) {
			var input = oEvent.getSource();
			input.setDescription("");
			var username = input.getValue();
			username = username.replace(/\s+/g, '');
			input.setValue(username);
			if (username.length > 0 && username.length < 9 && !isNaN(username)) {
				//input.bindElement("/AmeiSearchSet('" + username + "')");
			}

			//var qualModel = this.getView().getModel("qualModel");
			var ameiSearchModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZGW_MASS_LOAD_QUALIFICATIONS_SRV/");
			var oData = ameiSearchModel.read("/AmeiSearchSet('" + username + "')", {
				success: this.mySuccessHandlerSearch.bind(this),
				error: this.myErrorHandlerSearch.bind(this)
			});

		},

		mySuccessHandlerSearch: function (oData) {
			//
			this.getView().byId("inputAmei").setDescription(oData.Name);
		},
		myErrorHandlerSearch: function (oError) {
			//
		},
		onDelete: function (oEvent) {
			var item = oEvent.getSource();
			var sPath = item.getBindingContext("resModel").getPath();

			this.getView().getModel("qualModel").remove(sPath, {
				success: this.mySuccessHandlerRemove.bind(this),
				error: this.myErrorHandlerRemove.bind(this)
			});

		},

		mySuccessHandlerRemove: function (oData) {
			//
			var oTable = this.getView().byId("qualTable");
			oTable.getBinding("items").refresh();
		},

		myErrorHandlerRemove: function (oError) {
			//

		},
		onAddRow: function (oEvent) {

			var addDialog = this.byId("addDialog");

			this.byId("inputAmei").setValue("");
			this.byId("inputAmei").setDescription("");
			this.byId("qualListNew").setValue("");
			this.byId("dateNew").setValue("");

			var qualSearchModel = this.getView().byId("qualListNew").getModel("qualSearchModel");
			if (qualSearchModel === undefined || qualSearchModel === null) {

				qualSearchModel = new sap.ui.model.odata.v2.ODataModel("/sap/opu/odata/sap/ZGW_MASS_LOAD_QUALIFICATIONS_SRV/");

				var oData = qualSearchModel.read("/QualSearchSet()", {
					success: this.mySuccessHandlerNew.bind(this),
					error: this.myErrorHandlerNew.bind(this)
				});
			} else {

				addDialog.open();
			}
		},

		mySuccessHandlerEdit: function (oData) {
			//bind the oData to the dropdown box...
			var qualSearchModel = new sap.ui.model.json.JSONModel();
			qualSearchModel.setData(oData);
			this.getView().byId("qualList").setModel(qualSearchModel, "qualSearchModel");
			this.getView().byId("qualListNew").setModel(qualSearchModel, "qualSearchModel");
			this.byId("editDialog").open();
		},

		myErrorHandlerEdit: function (oError) {
			//
		},

		mySuccessHandlerNew: function (oData) {
			//bind the oData to the dropdown box...
			var qualSearchModel = new sap.ui.model.json.JSONModel();
			qualSearchModel.setData(oData);
			this.getView().byId("qualListNew").setModel(qualSearchModel, "qualSearchModel");
			this.getView().byId("qualList").setModel(qualSearchModel, "qualSearchModel");
			this.byId("addDialog").open();

		},

		myErrorHandlerNew: function (oError) {},

		handleSaveNew: function (oEvent) {
			//
			if (this.byId("inputAmei").getValue() === "" || this.byId("dateNew").getValue() === "" || this.byId("qualListNew").getValue() ===
				"") {
				alert("FILL IN MANDATORY FIELDS");
			} else {

				var qualModel = this.getView().getModel("qualModel");
				qualModel.setRefreshAfterChange(true);
				qualModel.setDeferredGroups(["qual"]);

				sap.ui.core.BusyIndicator.show();

				var obj = {};
				obj.Otype = "CP";
				obj.Objid = this.byId("inputAmei").getValue().toString();
				obj.QId = this.byId("qualListNew").getSelectedKey();
				obj.DateQuali = this.byId("dateNew").getValue().split("-").reverse().join("");
				qualModel.create("/MassLoadQualificationSet", obj, {
					groupId: "qual"
				});

				qualModel.submitChanges({
					groupId: "qual",
					success: this.mySuccessHandler.bind(this),
					error: this.myErrorHandler.bind(this)

				});

				this.byId("addDialog").close();
			}

		},

		handleCancelNew: function (oEvent) {
			//
			this.byId("addDialog").close();
		}

	});
});