/* global QUnit*/

sap.ui.define([
	"sap/ui/test/Opa5",
	"am/qualificationUpload/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"am/qualificationUpload/test/integration/pages/Qual",
	"am/qualificationUpload/test/integration/navigationJourney"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "am.qualificationUpload.view.",
		autoWait: true
	});
});